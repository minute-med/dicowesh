#ifndef _PROMPT__
# define _PROMPT__

char	*your_readline(void);
void	my_putstr(char *str);
int	my_getnbr(char *str);
void	*check_input_str(char *str);
void	*check_input_nbr(char *str);

#endif	/* _PROMPT__ */
