#ifndef _WRITE_DICO_TO_FILE__
# define _WRITE_DICO_TO_FILE__

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "list.h"

char	*my_strcat(char *dest, char *src);
int	my_strlen(char *str);

#endif	/* _WRITE_DICO_TO_FILE__ */
