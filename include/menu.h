#ifndef _MENU__
# define _MENU__

#include "list.h"

typedef struct s_fptr_list
{
  int			id;
  char			*name;
  char			*description;
  struct s_fptr_list	*next;
  int			(*action)();

} t_fptr_list;

void		display_menu(t_fptr_list **menu);
t_fptr_list	*fill_and_get_menu(void);
void		my_putstr(char *str);
char		*my_strdup(char *str);
int		my_strlen(char *str);
int		item_exist(t_fptr_list *menu, int id);
int		item_add_word(t_list_container **dico);
int		wesh_to_fr(t_list_container **dico);
int		fr_to_wesh(t_list_container **dico);
int		print_dict_fr(t_list_container *dico);
int		print_dict_wesh(t_list_container *dico);
int		quit(t_list_container **dico);
int		prompt_nbr(char *msg, void *(*check_input_nbr)());
int		item_remove_word(t_list_container **dico);
int		putstr_bgcolor(char *str, char *bgcolor_code);
char		*prompt_str_ucanon(char *msg);

#endif	/* _MENU__  */
