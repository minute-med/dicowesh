#ifndef MAIN_H_
# define MAIN_H_

#include "../include/list.h"
#include "../include/menu.h"
#include "../include/dico.h"
#include "../include/prompt.h"

char		**explode(char *str, char c);
char		*my_readfile(char *path);
void		free_menu(t_fptr_list **menu);
void		free_dico(t_list_container **dico);
int		write_dico_to_file(t_list_container *dico, char *filepath);
int		fill_dico_from_file(t_list_container **dico, char *path);
int		prompt_menu(t_fptr_list *menu,
			    t_list_container **dico,
			    char *welcome_msg);
int		prompt_nbr(char *msg, void *(*check_input_nbr)());
t_fptr_list	*my_rev_menu(t_fptr_list **menu);

#endif /* !MAIN_H_ */
