#ifndef _MENU_ITEM__
# define _MENU_ITEM__

#include <stdlib.h>
#include "list.h"

int	item_add_word(t_list_container **dico);
void	my_putstr(char *str);
char	*prompt_str(char *msg, void *(*check_input_str)());
int	my_strlen(char *);
void	*add_word(t_list_container **dico, t_word *word);
t_word	*get_word(char *fr, char *fr_def, char *wesh, char *wesh_def);

#endif	/* _MENU_ITEM__ */
