#ifndef _DICO__
# define _DICO__

#include "list.h"

t_list_container *	get_dico_list(void);
int			fill_dict(t_list_container **dico);
t_word			*find_word_by(t_list_container *dico,
				      char *needle,
				      char *member);
t_word			**find_words_by(t_list_container *dico,
					char *needle,
					char *member);

#endif
