#ifndef _UTILS_H_
# define _UTILS_H_

#include "list.h"
#include "menu.h"

t_word			*find_word_by(t_list_container *dico,
				      char *needle,
				      char *member);
void			my_putstr(char *str);

#endif /* !UTILS_H_ */
