#ifndef _TRADUCTION__
# define _TRADUCTION__

#include "../include/list.h"

void	my_putstr(char *);
char	*prompt_str(char *msg, void *(*check_str)());
void	*is_alpha_word(char *);
int	my_strcmp(char *, char *);
void	*is_y_or_n(char *);
int	putstr_color(char *str, char *color_code);

#endif	/* _TRADUCTION__ */
