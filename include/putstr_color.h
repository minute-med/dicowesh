/*
** putstr_color.h for putstr_color.h in /home/touba_e/Desktop/dico/sv_dico/touba_e
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 05:03:00 2016 TOUBA Erwan
** Last update Sat Apr  9 05:19:18 2016 TOUBA Erwan
*/

#ifndef PUTSTR_COLOR_H_
# define PUTSTR_COLOR_H_

# define TXT_RED	"\e[31m"
# define TXT_GREEN	"\e[32m"
# define TXT_YELLOW	"\e[33m"
# define TXT_BLUE	"\e[34m"
# define TXT_WHITE	"\e[97m"
# define TXT_DEFAULT	"\e[39m"

# define BG_RED		"\e[41m"
# define BG_GREEN	"\e[42m"
# define BG_YELLOW	"\e[43m"
# define BG_BLUE	"\e[44m"
# define BG_WHITE	"\e[107m"
# define BG_DEFAULT	"\e[49m"

#endif /* !PUTSTR_COLOR_H_ */
