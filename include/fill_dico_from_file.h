#ifndef _WRITE_DICO_TO_FILE__
# define _WRITE_DICO_TO_FILE__

#include "list.h"

char	**explode(char *str, char delim);
int	is_alpha_word(char *str);
int	is_alpha_string(char *str);
t_word	*get_word(char *, char *, char *, char *);
void	*add_word(t_list_container **dico, t_word *word);
void	free_2d_tab(char **tab);

#endif	/* _WRITE_DICO_TO_FILE__ */
