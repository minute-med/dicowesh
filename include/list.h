#ifndef _LIST__
# define _LIST__

typedef struct	s_word
{
  char		*fr;
  char		*fr_def;
  char		*wesh;
  char		*wesh_def;

} t_word;

typedef struct	s_list_node
{
  struct s_list_node	*next;
  t_word		*word;

} t_list_node;

typedef struct	s_container
{
  t_list_node	*begin;
  int		(*getsize)();

} t_list_container;

char	*my_strdup(char *str);
t_word *find_word_by(t_list_container *dico, char *needle, char *member);

#endif
