/*
** fill_dico_from_file.c for fill_dico_from_file.c in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:21:32 2016 TOUBA Erwan
** Last update Sat Apr  9 13:38:40 2016 TOUBA Erwan
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

#include "../include/fill_dico_from_file.h"

int	count_str_tab(char **tab)
{
  int	i;

  i = -1;
  while (tab[++i]);
  return (i);
}

t_word		*get_word_from_line(char *line)
{
  char		**word_tab;
  t_word	*w;

  word_tab = 0;
  word_tab = explode(line, ';');
  if (count_str_tab(word_tab) < 4)
    {
      free_2d_tab(word_tab);
      return (NULL);
    }
  if (is_alpha_word(word_tab[0]) < 0
      || is_alpha_string(word_tab[1]) < 0
      || is_alpha_word(word_tab[2]) < 0
      || is_alpha_string(word_tab[3]) < 0)
    {
      free_2d_tab(word_tab);
      return (NULL);
    }
  w = get_word(word_tab[0], word_tab[1], word_tab[2], word_tab[3]);
  free(word_tab);
  return (w);
}

int	get_file_size(int fd)
{
  struct stat stats;
  return (fstat(fd, &stats) == 0 ? stats.st_size : -1);
}

void	my_putstr(char *);

int	fill_dico_from_file(t_list_container **dico, char *path)
{
  int	fd;
  int	fsize;
  int	i;
  char *f_str;
  char **word_lines;

  i = 0;
  if ((fd = open(path, O_RDONLY)) < 0 || (fsize = get_file_size(fd)) < 0)
    return (-1);
  if ((f_str = malloc(sizeof(char) * fsize + 1)) == NULL)
    return (-1);
  if (read(fd, f_str, fsize) < 0)
    return (-1);
  f_str[fsize] = 0;
  if((word_lines = explode(f_str, '\n')) == NULL)
    {
      free(f_str);
      return (-1);
    }
  while (word_lines[i])
    {
      add_word(dico, get_word_from_line(word_lines[i]));
      free(word_lines[i]);
      ++i;
    }
  free(word_lines);
  return (0);
}
