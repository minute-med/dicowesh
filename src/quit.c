/*
** quit.c for quit.c in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:25:39 2016 TOUBA Erwan
** Last update Sat Apr  9 00:25:39 2016 TOUBA Erwan
*/

#include "../include/list.h"

void	my_putstr(char *str);

int	quit(t_list_container **dico)
{
  dico = dico;
  my_putstr("Bye !\n");
  return (1);
}
