/*
** main.c for main.c in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:25:00 2016 TOUBA Erwan
** Last update Sat Apr  9 15:31:04 2016 TOUBA Erwan
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>

#include "../include/main.h"
#include "../include/putstr_color.h"

int			main(void)
{
  t_list_container	*dico;
  t_fptr_list		*menu;
  char			*welcome_msg;

  dico = get_dico_list();
  welcome_msg = my_readfile("./welcome.txt");
  if (dico == 0)
    return (-1);
  fill_dico_from_file(&dico, "./dicts/default");
  menu = fill_and_get_menu();
  menu = my_rev_menu(&menu);
  while ((prompt_menu(menu, &dico, welcome_msg)) == 0);
  write_dico_to_file(dico, "./dicts/default");
  free_menu(&menu);
  free_dico(&dico);
  free(welcome_msg);
  return (0);
}
