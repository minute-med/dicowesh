/*
** fr_to_wesh.c for fr_to_wesh in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:21:46 2016 TOUBA Erwan
** Last update Sat Apr  9 13:03:57 2016 TOUBA Erwan
*/

#include <stdlib.h>
#include "../include/traduction.h"
#include "../include/putstr_color.h"

void	print_fr_to_wesh(t_list_container **dico, char *fr_word)
{
  t_list_node	*begin;
  int		found;

  found = 0;
  begin = (*dico)->begin;
  while ((*dico)->begin)
    {
      if (my_strcmp((*dico)->begin->word->fr, fr_word) == 0)
	{
	  my_putstr("Traduction : ");
	  my_putstr((*dico)->begin->word->wesh);
	  my_putstr("\n");
	  found = 1;
	}
      (*dico)->begin = (*dico)->begin->next;
    }
  (*dico)->begin = begin;
  if(found == 0)
    putstr_color("Mot inconnu\n", TXT_RED);
}

int		fr_to_wesh(t_list_container **dico)
{
  char		*fr_word;
  char		*exit_choice;
  int		choice;

  fr_word = prompt_str("Entrer un mot Fr :\n>", &is_alpha_word);
  print_fr_to_wesh(dico, fr_word);
  exit_choice = prompt_str("Retourner au menu ?\n[y/n]>", &is_y_or_n);
  choice = exit_choice[0];
  free(exit_choice);
  free(fr_word);
  if (choice == 'y' || choice == 'Y')
    return (0);
  return (1);
}
