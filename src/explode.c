/*
** explode.c for explode.c in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:21:14 2016 TOUBA Erwan
** Last update Sat Apr  9 11:52:02 2016 TOUBA Erwan
*/

#include <stdlib.h>
#include <stdio.h>

int	my_strlen(char *);

char	*my_strnmdup(char *str, int n, int m)
{
  char	*ptr;
  int	i;

  ptr = 0;
  i = 0;
  if (NULL == (ptr = malloc(sizeof(char) * (m - n + 1) + 1)))
    return (NULL);
  while (n <= m)
    {
      ptr[i] = str[n];
      ++n;
      ++i;
    }
  ptr[i] = 0;
  return (ptr);
}

int     delim_wordlen(char *str, char delim)
{
  int	i;
  int	nb_word;

  i = 0;
  nb_word = 0;
  while (str[i])
    {
      if (str[i] != delim)
	{
	  ++nb_word;
	  while (str[i] != delim && str[i] != 0)
	    ++i;
	}
      else
	++i;
    }
  return (nb_word);
}

char	**explode(char *str, char delim)
{
  int	i;
  int	j;
  int	k;
  char	**tab;

  i = 0;
  j = 0;
  k = -1;
  tab = 0;
  if (delim_wordlen(str, delim) < 1)
    return (NULL);
  if ((tab = malloc(sizeof(char *) * delim_wordlen(str, delim) + 1)) == NULL)
    return (NULL);
  while (str[i])
    {
      if (str[i] != delim)
	{
	  j = i;
	  while (str[i] != delim && str[i] != 0)
	    ++i;
	  tab[++k] = my_strnmdup(str, j, (i + 1 == my_strlen(str) ? i: i - 1));
	}
      else
	++i;
    }
  tab[k] = 0;
  return (tab);
}
