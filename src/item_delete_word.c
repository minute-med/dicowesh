/*
** item_delete_word.c for item_delete_word.c in /home/touba_e/Desktop/dico/sv_dico/touba_e
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 01:10:30 2016 TOUBA Erwan
** Last update Sat Apr  9 06:33:11 2016 TOUBA Erwan
*/

#include "../include/menu_item.h"

void	*is_alpha_word(char *str);
int	my_strcmp(char *st1, char *st2);
void	*is_y_or_n(char *str);

void	free_dico_node(t_list_node *node)
{
  free(node->word->fr);
  free(node->word->fr_def);
  free(node->word->wesh);
  free(node->word->wesh_def);
  free(node->word);
  free(node);
}

int	delete_word(t_list_container **dico,char *fr_word_to_delete)
{
  t_list_node	*prev;
  t_list_node	*start;

  prev = 0;
  start = (*dico)->begin;
  while ((*dico)->begin)
    {
      if (my_strcmp(fr_word_to_delete, (*dico)->begin->word->fr) == 0)
	{
	  if (prev == 0)
	    {
	      start = 0;
	      free_dico_node((*dico)->begin);
	    }
	  else if ((*dico)->begin->next == 0)
	    {
	      prev->next = 0;
	      free_dico_node((*dico)->begin);
	    }
	  else
	    {
	      prev->next = (*dico)->begin->next;
	      free_dico_node((*dico)->begin);
	    }
	  break;
	}
      prev = (*dico)->begin;
      (*dico)->begin = (*dico)->begin->next;
    }
  (*dico)->begin = start;
  return (0);
}

int		item_remove_word(t_list_container **dico)
{
  char		*word_to_delete;
  char		*exit_choice;
  char		choice;

  word_to_delete = prompt_str("Entrer le mot fr a supprimer:", &is_alpha_word);
  delete_word(dico, word_to_delete);
  exit_choice = prompt_str("Retourner au menu ?\n[y/n]>", &is_y_or_n);
  choice = exit_choice[0];
  free(exit_choice);
  free(word_to_delete);
  return (choice == 'y' || choice == 'Y' ? 0 : 1);
}
