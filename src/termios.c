/*
** termios.c for termios.c in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:23:04 2016 TOUBA Erwan
** Last update Sat Apr  9 10:28:40 2016 TOUBA Erwan
*/

#include <termios.h>
#include <unistd.h>
#include<stdlib.h>
#include "../include/prompt.h"

void	init_termios_struct(struct termios *term)
{
  tcgetattr(0, term);
}

void	set_canon(struct termios *term)
{
  term->c_lflag &= ICANON;
  tcsetattr(0, 0, term);
}

void	unset_canon(struct termios *term)
{
  term->c_lflag &= ~ICANON;
  term->c_cc[VMIN] = 1;
  term->c_cc[VTIME] = 0;
  tcsetattr(0, TCSAFLUSH, term);
}

char	*getline_uncanon()
{
  char	*buff;
  char	tmp_buff[1];
  int   i;

  i = -1;
  if ((buff = malloc(sizeof(*buff) * (50 + 1))) == NULL)
    return (NULL);
  while (42)
    {
      if (read(0, tmp_buff, 1) < 1)
	return (NULL);
      if (tmp_buff[0] == 9 || tmp_buff[0] == 11 || tmp_buff[0] == 11)
	{
	  my_putstr("YOLO4");
	  my_putstr("\b\b\b   \b\b\b");
	}
      else if (tmp_buff[0] == 127)
	{
	  my_putstr("YOLO3");
	  my_putstr("\b\b\b   \b\b\b");
	}
      else if (tmp_buff[0] == '\n')
	break;
      else if((tmp_buff[0] >= 'a' &&  tmp_buff[0] <= 'z')
	      || (tmp_buff[0] >= 'A' &&  tmp_buff[0] <= 'Z')
	      || tmp_buff[i] != ' ')
	{
	  buff[++i] = tmp_buff[0];
	}
    }
  buff[++i] = 0;
  return (buff);
}

char			*prompt_str_ucanon(char *msg)
{
  struct termios	term;
  char			*word;

  word = 0;
  my_putstr(msg);
  init_termios_struct(&term);
  unset_canon(&term);
  word = getline_uncanon();
  set_canon(&term);
  my_putstr("the word we want to submit:'");
  my_putstr(word);
  my_putstr("'\n");
  return (word);
}
