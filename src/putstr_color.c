/*
** putstr_color.c for putstr_color.c in /home/touba_e/Desktop/dico/sv_dico/touba_e
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 04:58:19 2016 TOUBA Erwan
** Last update Sat Apr  9 05:58:38 2016 TOUBA Erwan
*/

#include "../include/putstr_color.h"

void	my_putstr(char *);

int	putstr_color(char *str, char *color_code)
{
  if(str == 0)
    return (0);
  my_putstr(color_code);
  my_putstr(str);
  my_putstr("\e[0m");
  return (1);
}

int	putstr_bgcolor(char *str, char *color_code)
{
  if(str == 0)
    return (0);
  my_putstr(color_code);
  my_putstr(str);
  my_putstr("\e[0m");
  return (1);
}
