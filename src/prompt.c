/*
** prompt.c for prompt.c in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:25:12 2016 TOUBA Erwan
** Last update Sat Apr  9 15:38:55 2016 TOUBA Erwan
*/

#include <stdlib.h>
#include "../include/prompt.h"

char	*prompt_str(char *msg, void *(*check_input_str)())
{
  char	*input;
  char  *err_msg;

  input = 0;
  err_msg = 0;
  do {
    if (err_msg == 0)
      my_putstr(msg);
    else
      my_putstr(err_msg);
    free(input);
    input = your_readline();
    err_msg = check_input_str(input);
  }
  while (err_msg != 0);
  return (input);
}

int	prompt_nbr(char *msg, void *(*check_input_nbr)())
{
  char	*input;
  int	tmp_nb;
  char  *err_msg;

  input = 0;
  tmp_nb = 0;
  err_msg = 0;
  do {
    if (err_msg == 0)
      my_putstr(msg);
    else
      {
	my_putstr(err_msg);
	free(err_msg);
	err_msg = 0;
      }
    free(input);
    input = your_readline();
    err_msg = check_input_nbr(input);
  }
  while (err_msg != 0);
  tmp_nb = my_getnbr(input);
  free(input);
  return (tmp_nb);
}
