/*
** input_validation.c for input_validation.c in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:24:21 2016 TOUBA Erwan
** Last update Sat Apr  9 03:21:50 2016 TOUBA Erwan
*/

void		*check_input_str(char *str)
{
  int		i;
  static char	err_msg[64] = "Veuillez entrer uniquement des caracteres alphabetiques svp\n>";

  i = -1;
  if (str == 0 || str[0] == '\0'  || str[0] == '\n')
    return ((void *)err_msg);
  while (str[++i])
    {
      if (((str[i] < 'A' && str[i] > 'Z') || (str[i] < 'a' && str[i] > 'z')))
	return ((void *)(err_msg));
    }
  return (0);
}

void		*check_input_nbr(char *str)
{
  int		i;
  static char	err_msg[45] = "Veuillez entrer uniquement des nombres svp\n>";

  i = 0;
  if (str == 0 || str[0] == '\0')
    return ((void *)err_msg);
  while (str[i] == '+' || str[i] == '-')
    ++i;
  while (str[++i])
    if (str[i] < '0' || str[i] > '9')
      return ((void *)err_msg);
  return (0);
}
