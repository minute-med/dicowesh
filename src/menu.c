/*
** menu.c for menu.c in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:25:31 2016 TOUBA Erwan
** Last update Sat Apr  9 15:37:20 2016 TOUBA Erwan
*/

#include <stdlib.h>
#include "../include/menu.h"
#include "../include/prompt.h"
#include "../include/putstr_color.h"

/*
** todo: sort_list
*/
t_fptr_list *my_rev_menu(t_fptr_list **menu)
{
  t_fptr_list *prev;
  t_fptr_list *curr;
  t_fptr_list *nxt;

  prev = 0;
  curr = (*menu);
  nxt = 0;
  while (curr)
    {
      nxt = curr->next;
      curr->next = prev;
      prev = curr;
      curr = nxt;
    }
  *menu = prev;
  return (*menu);
}

void		display_menu(t_fptr_list **begin)
{
  t_fptr_list	*start;

  start = *begin;
  while (*begin)
    {
      my_putstr((*begin)->name);
      my_putstr((*begin)->description);
      my_putstr("\n");
      *begin = (*begin)->next;
    }
  *begin = start;
}

t_fptr_list	*create_menu_item(char *item_name,
				  char *description,
				  int (*action)())
{
  t_fptr_list	*item;
  static  int	id = 0;

  if (((item) = malloc(sizeof(struct s_fptr_list))) == NULL)
    return (NULL);
  item->next = 0;
  item->id = ++id;
  item->name = my_strdup(item_name);
  item->description = my_strdup(description);
  item->action = action;
  return (item);
}

void	push_item_to_menu_list(t_fptr_list **begin, t_fptr_list *item)
{
  item->next = *begin;
  *begin = item;
}

t_fptr_list	*fill_and_get_menu(void)
{
  t_fptr_list	*begin;
  t_fptr_list	*item1;
  t_fptr_list	*item2;
  t_fptr_list	*item3;
  t_fptr_list	*item4;
  t_fptr_list	*item5;
  t_fptr_list	*item6;
  t_fptr_list	*item7;

  begin = 0;
  item1 = create_menu_item("1.", "Ajouter un mot", &item_add_word);
  push_item_to_menu_list(&begin, item1);
  item2 = create_menu_item("2.", "Supprimer un mot", &item_remove_word);
  push_item_to_menu_list(&begin, item2);
  item3 = create_menu_item("3.", "Traduire fr : wesh", &fr_to_wesh);
  push_item_to_menu_list(&begin, item3);
  item4 = create_menu_item("4.", "Traduire wesh : fr", &wesh_to_fr);
  push_item_to_menu_list(&begin, item4);
  item5 = create_menu_item("5.", "Afficher fr : wesh", &print_dict_fr);
  push_item_to_menu_list(&begin, item5);
  item6 = create_menu_item("6.", "Afficher wesh : fr", &print_dict_wesh);
  push_item_to_menu_list(&begin, item6);
  item7 = create_menu_item("7.", "Quit", &quit);
  push_item_to_menu_list(&begin, item7);
  return (begin);
}

/*
** cette fonction affiche le menu a l'utilisateur
** si l'utilisateur veux retourner au menu et continuer le program : return 0
** sinon : return 1
*/
int	prompt_menu(t_fptr_list *menu, t_list_container **dico, char *welcome)
{
  int	choice;

  choice = 0;
  system("clear");
  my_putstr(welcome);
  putstr_bgcolor("\nVeuillez choisir une option ci-dessous\n", BG_BLUE);
  display_menu(&menu);
  choice = prompt_nbr(">", &check_input_nbr);
  while (menu)
    {
      if (choice == (menu)->id)
	return (menu->action(dico));
      menu = (menu)->next;
    }
  return (1);
}
