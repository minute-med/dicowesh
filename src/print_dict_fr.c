/*
** print_dict_fr.c for print_dict_fr.c in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:22:39 2016 TOUBA Erwan
** Last update Sat Apr  9 14:39:11 2016 TOUBA Erwan
*/

#include <stdlib.h>
#include "../include/list.h"

void		my_putstr(char *str);
char		*prompt_str(char *str,void *(*check_str)());
void		*is_y_or_n(char *str);
t_list_node	*my_sort_dico_fr(t_list_container **dico);

int		print_dict_fr(t_list_container **dico)
{
  char		choice;
  t_list_node	*begin;
  char		*exit_choice;

  begin = (*dico)->begin;
  while ((*dico)->begin)
    {
      my_putstr((*dico)->begin->word->fr);
      my_putstr(" : ");
      my_putstr((*dico)->begin->word->wesh);
      my_putstr("\n");
      (*dico)->begin = (*dico)->begin->next;
    }
  (*dico)->begin = begin;
  exit_choice = prompt_str("Retourner au menu ?\n(y/n)>", &is_y_or_n);
  choice = exit_choice[0];
  free(exit_choice);
  if (choice == 'y' || choice == 'Y')
    return (0);
  return (1);
}
