/*
** wesh_to_fr.c for wesh_to_fr.c in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:23:32 2016 TOUBA Erwan
** Last update Sat Apr  9 06:24:38 2016 TOUBA Erwan
*/

#include <stdlib.h>
#include "../include/traduction.h"
#include "../include/putstr_color.h"

void	print_wesh_to_fr(t_list_container **dico, char *wesh_word)
{
  t_list_node	*begin;
  int		found;

  found = 0;
  begin = (*dico)->begin;
  while ((*dico)->begin)
    {
      if (my_strcmp((*dico)->begin->word->wesh, wesh_word) == 0)
	{
	  my_putstr("Traduction : ");
	  my_putstr((*dico)->begin->word->fr);
	  my_putstr("\n");
	  found = 1;
	}
      (*dico)->begin = (*dico)->begin->next;
    }
  (*dico)->begin = begin;
  if(found == 0)
    putstr_color("Mot inconnu\n", TXT_RED);
}


int		wesh_to_fr(t_list_container **dico)
{
  char		*wesh_word;
  char		*exit_choice;
  int		choice;

  wesh_word = prompt_str("Entrer un mot wesh :\n>", &is_alpha_word);
  print_wesh_to_fr(dico, wesh_word);
  exit_choice = prompt_str("Retourner au menu ?\n[y/n]>", &is_y_or_n);
  choice = exit_choice[0];
  free(exit_choice);
  free(wesh_word);
  if (choice == 'y' || choice == 'Y')
    return (0);
  return (1);
}
