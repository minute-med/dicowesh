/*
** list.c for list.c in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:24:10 2016 TOUBA Erwan
** Last update Sat Apr  9 14:19:55 2016 TOUBA Erwan
*/

#include <stdlib.h>
#include <string.h>
#include "../include/list.h"

int		getsize(t_list_container *dico)
{
  int		i;
  t_list_node	*start;

  i = 0;
  start = dico->begin;
  while (dico->begin)
    {
      ++i;
      dico->begin = dico->begin->next;
    }
  dico->begin = start;
  return (i);
}

t_list_container *get_dico_list(void)
{
  t_list_container *dico;

  if ((dico = malloc(sizeof(t_list_container))) == NULL)
    return (NULL);
  dico->getsize = &getsize;
  dico->begin = 0;
  return (dico);
}

void	override_word(t_word *found_word, t_word *new_word)
{
  free(found_word->fr);
  free(found_word->fr_def);
  free(found_word->wesh);
  free(found_word->wesh_def);

  found_word->fr = my_strdup(new_word->fr);
  found_word->fr_def = my_strdup(new_word->fr_def);
  found_word->wesh = my_strdup(new_word->wesh);
  found_word->wesh_def = my_strdup(new_word->wesh_def);

  free(new_word->fr);
  free(new_word->fr_def);
  free(new_word->wesh);
  free(new_word->wesh_def);
  free(new_word);
}

void		*add_word(t_list_container **dico, t_word *word)
{
  t_list_node	*new_node;
  t_word	*found_word;

  if (word == 0)
    return (NULL);

  if ((found_word = find_word_by(*dico, word->fr, "fr")) != 0)
    {
      override_word(found_word, word);
      return (0);
    }
  if ((new_node = malloc(sizeof(struct s_list_node))) == NULL)
    return (NULL);
  new_node->word = word;
  new_node->next = (*dico)->begin;
  (*dico)->begin = new_node;
  return (0);
}
