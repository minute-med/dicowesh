/*
** utils.c for utils.c in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:23:14 2016 TOUBA Erwan
** Last update Sat Apr  9 13:19:24 2016 TOUBA Erwan
*/

#include <stdlib.h>
#include "../include/utils.h"

void	free_menu(t_fptr_list **menu)
{
  t_fptr_list *m_prev;

  while (*menu)
    {
      m_prev = *menu;
      free((*menu)->name);
      free((*menu)->description);
      *menu = (*menu)->next;
      free(m_prev);
    }
  free(*menu);
}

void	free_2d_tab(char **tab)
{
  int	i;

  i = 0;
  while(tab[i])
    {
      free(tab[i]);
      ++i;
    }
  free(tab);
}

void		free_dico(t_list_container **dico)
{
  t_list_node	*prev;

  prev = 0;
  while ((*dico)->begin)
    {
      prev = (*dico)->begin;
      free((*dico)->begin->word->fr);
      free((*dico)->begin->word->fr_def);
      free((*dico)->begin->word->wesh);
      free((*dico)->begin->word->wesh_def);
      free((*dico)->begin->word);
      (*dico)->begin = (*dico)->begin->next;
      free(prev);
    }
  free(*dico);
}

void	autocomplete(t_list_container *dico,char *needle, char *member)
{
  t_word *word;

  word = find_word_by(dico, needle, member);
  if (word == 0)
    return;
  my_putstr("\n");
  my_putstr(word->fr);
  my_putstr("\n");
  my_putstr(needle);
}
