/*
** print_dict.c for print_dict.c in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:22:25 2016 TOUBA Erwan
** Last update Sat Apr  9 00:22:26 2016 TOUBA Erwan
*/

#include "../include/list.h"

void	my_putstr(char *str);
char	*prompt_str(char *str, int (*check_str)());
void	*is_y_or_n(char *str);

int		print_dict_fr(t_list_container **dico)
{
  t_list_node	*begin;
  char		*exit_choice;

  begin = (*dico)->begin;
  while ((*dico)->begin)
    {
      my_putstr((*dico)->begin->word->fr);
      my_putstr(" : ");
      my_putstr((*dico)->begin->word->wesh);
      my_putstr("\n");
      (*dico)->begin = (*dico)->begin->next;
    }
  (*dico)->begin = begin;
  exit_choice = prompt_str("Retourner au menu ?\n(y/n)>", &is_y_or_n);
  return (exit_choice[0] == 'y' || exit_choice[0] == 'Y' ? 0 : 1);
}
