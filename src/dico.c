/*
** dico.c for dico.c in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:20:52 2016 TOUBA Erwan
** Last update Sat Apr  9 17:48:38 2016 TOUBA Erwan
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../include/list.h"

int	getsize(t_list_container *dico);
void	*add_word(t_list_container **dico, t_word *word);
char	*my_strdup(char *str);
int	my_strcmp(char *, char *);
void	my_putstr(char *str);

t_word		*find_word_by(t_list_container *dico, char *needle, char *member)
{
  t_list_node	*start;

  start = (dico)->begin;
  while (dico->begin)
    {
      if ((my_strcmp(member, "fr") == 0)
	  && (strstr(dico->begin->word->fr, needle) != 0))
	return (dico->begin->word);
      else if ((my_strcmp(member, "fr_def") == 0)
	       && (strstr(dico->begin->word->fr_def, needle) != 0))
	return (dico->begin->word);
      else if ((my_strcmp(member, "wesh") == 0)
	       && (strstr(dico->begin->word->wesh, needle) != 0))
	return (dico->begin->word);
      else if ((my_strcmp(member, "wesh_def") == 0)
	       && (strstr(dico->begin->word->wesh_def, needle) != 0))
	return (dico->begin->word);
      dico->begin = dico->begin->next;
    }
  (dico)->begin = start;
  return (NULL);
}

t_word *find_words_by(t_list_container *dico, char *needle, char *member)
{
  while (dico->begin)
    {
      if ((my_strcmp(member, "fr") == 0)
	  && (strstr(dico->begin->word->fr, needle) != 0))
	return (dico->begin->word);
      else if ((my_strcmp(member, "fr_def") == 0)
	       && (strstr(dico->begin->word->fr_def, needle) != 0))
	return (dico->begin->word);
      else if ((my_strcmp(member, "wesh") == 0)
	       && (strstr(dico->begin->word->wesh, needle) != 0))
	return (dico->begin->word);
      else if ((my_strcmp(member, "wesh_def") == 0)
	       && (strstr(dico->begin->word->wesh_def, needle) != 0))
	return (dico->begin->word);
      dico->begin = dico->begin->next;
    }
  return (NULL);
}

t_word		*get_word(char *fr, char *fr_def, char *wesh, char *wesh_def)
{
  t_word	*word;

  word = 0;
  if ((word = malloc(sizeof(struct s_word))) == NULL)
    return (NULL);
  word->fr = my_strdup(fr);
  word->fr_def = my_strdup(fr_def);
  word->wesh = my_strdup(wesh);
  word->wesh_def = my_strdup(wesh_def);
  return (word);
}

int		fill_dict(t_list_container **dico)
{
  t_word	*word;
  t_word	*word2;

  word = 0;
  word2 = 0;
  word = get_word("bonjour","wesh bien ou quoi ?","wesh","salutations matinal");
  word2 = get_word("aurevoir","vazi?","azi","bye bye");
  add_word(dico, word);
  add_word(dico, word2);
  return (1);
}
