/*
** write_dico_to_file.c for write_dico_to_file.c in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:24:47 2016 TOUBA Erwan
** Last update Sat Apr  9 12:54:28 2016 TOUBA Erwan
*/

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "../include/write_dico_to_file.h"

/*
** fonction qui compte le nombre de caracteres dans une variable de type t_word
** elle ajoute les ;
*/
int	t_word_len(t_word *w)
{
  int	c;

  c = my_strlen(w->fr);
  c += my_strlen(w->wesh);
  c += my_strlen(w->fr_def);
  c += my_strlen(w->wesh_def);
  return (c + 3);
}
void	my_putstr(char *);
char	*my_strcpy(char *s1, char *s2);
char	*get_line_from_word(t_word *word)
{
  char	*str;

  str = 0;

  if ((str = malloc(sizeof(char) * (t_word_len(word) + 2) )) == NULL)
    return (NULL);
  my_strcpy(str, word->fr);
  my_strcat(str, ";");
  my_strcat(str, word->fr_def);
  my_strcat(str, ";");
  my_strcat(str, word->wesh);
  my_strcat(str, ";");
  my_strcat(str, word->wesh_def);
  str[my_strlen(str) - 1] = '\n';
  str[my_strlen(str)] = '\0';
  return (str);
}

int	write_dico_to_file(t_list_container *dico, char *filepath)
{
  int	fd;
  char *tmp_line;

  tmp_line = 0;
  if ((fd = open(filepath, O_CREAT | O_WRONLY, S_IWUSR)) < 0)
    return (-1);
  while (dico->begin)
    {
      tmp_line = get_line_from_word(dico->begin->word);
      if (write(fd, tmp_line, my_strlen(tmp_line)) < 0)
	{
	  free(tmp_line);
	  return (-1);
	}
      free(tmp_line);
      tmp_line = 0;
      dico->begin = dico->begin->next;
    }
  close(fd);
  return (0);
}
