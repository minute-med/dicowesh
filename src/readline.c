/*
** readline.c for readline.c in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:25:50 2016 TOUBA Erwan
** Last update Sat Apr  9 04:43:50 2016 TOUBA Erwan
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdlib.h>

int	get_file_size(int fd);

char		*your_readline(void)
{
  ssize_t	ret;
  char		*buff;


  if ((buff = malloc(sizeof(*buff) * (50 + 1))) == NULL)
    return (NULL);
  if ((ret = read(0, buff, 50)) > 1)
    {
      buff[ret - 1] = '\0';
      return (buff);
    }
  buff[0] = '\0';
  return (buff);
}

char		*my_readfile(char *path)
{
  int		fd;
  int		fsize;
  ssize_t	ret;
  char		*buff;

  if ((fd = open(path, O_RDONLY)) < 0 || (fsize = get_file_size(fd)) < 0)
    return (NULL);
  if ((buff = malloc(sizeof(*buff) * fsize + 1)) == NULL)
    return (NULL);
  if ((ret = read(fd, buff, fsize)) > 1)
    {
      buff[ret] = '\0';
      return (buff);
    }
  buff[0] = '\0';
  return (buff);
}
