/*
** item_add_word.c for item_add_word.c in /home/touba_e/Desktop/dico/src
**
** Made by TOUBA Erwan
** Login   <touba_e@etna-alternance.net>
**
** Started on  Sat Apr  9 00:22:00 2016 TOUBA Erwan
** Last update Sat Apr  9 13:26:55 2016 TOUBA Erwan
*/

#include "../include/menu_item.h"

char	*my_strdup(char *);

void	*is_alpha_string(char *str)
{
  int	i;

  i = -1;
  while (str[++i])
    {
      if (((str[i] < 'A' || str[i] > 'Z')
	   && (str[i] < 'a' || str[i] > 'z'))
	  && str[i] != ' ')
	return (str[0] == 0 ? my_strdup("Entrer uniquement des lettres\n>"): 0);
    }
  return (0);
}

void	*is_alpha_word(char *str)
{
  int	i;

  i = -1;
  while (str[++i])
    {
      if ((str[i] < 'A' || str[i] > 'Z')
	  && (str[i] < 'a' || str[i] > 'z'))
	return (my_strdup("Entrer uniquement des lettres\n>"));
    }
  return (str[0] == 0 ? my_strdup("Entrer uniquement des lettres\n>"): 0);
}

void	*is_y_or_n(char *str)
{
  if ((str[0] != 'y' && str[0] != 'n'
       && str[0] != 'Y' && str[0] != 'N')
      || my_strlen(str) > 1)
    return (my_strdup("Entrer uniquement y ou n\n"));
  return (0);
}

int	item_add_word(t_list_container **dico)
{
  char	*fr;
  char	*fr_def;
  char *wesh;
  char *wesh_def;
  char *exit_choice;
  char	choice;

  fr = 0;
  fr_def = 0;
  wesh = 0;
  wesh_def = 0;
  wesh = prompt_str("Entrer le mot wesh :\n>", &is_alpha_word);
  fr = prompt_str("Entrer le mot fr :\n>", &is_alpha_word);
  fr_def = prompt_str("Entrer la definition fr :\n>", &is_alpha_string);
  wesh_def = prompt_str("Entrer la definition wesh :\n>", &is_alpha_string);
  add_word(dico, get_word(fr, fr_def, wesh, wesh_def));
  exit_choice = prompt_str("Retourner au menu ?\n[y/n]>", &is_y_or_n);
  choice = exit_choice[0];
  free(exit_choice);
  free(fr);
  free(fr_def);
  free(wesh);
  free(wesh_def);
  return (choice == 'y' || choice == 'Y' ? 0: 1);
}
