CC = gcc

SRC=		src/main.c \
		src/list.c \
		src/dico.c \
		src/prompt.c \
		src/readline.c \
		src/input_validation.c \
		src/item_add_word.c \
		src/item_delete_word.c \
		src/wesh_to_fr.c \
		src/fr_to_wesh.c \
		src/explode.c \
		src/print_dict_fr.c \
		src/print_dict_wesh.c \
		src/write_dico_to_file.c \
		src/fill_dico_from_file.c \
		src/quit.c \
		src/utils.c \
		src/putstr_color.c \
		src/menu.c

RM=		rm -f

NAME=		dicoWesh

OBJS=		$(SRC:.c=.o)

CFLAGS=		-W -Wall -Werror -Wextra -g3

LIB=		-L./libmy_03/ -lmy

$(NAME):	$(OBJS)
		$(CC) $(OBJS) $(LIB) -o $(NAME)

all:		$(NAME)

clean:
		$(RM) $(OBJS)

fclean:		clean
		$(RM) $(NAME)

re:		fclean all

.PHONY:		all clean fclean re
